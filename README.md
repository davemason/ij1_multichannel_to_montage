### Multichannel Image to Montage ###
A simple [Fiji](http://fiji.sc) script to convert a multichannel image into a montage (with merge).

Will take any number of channels (movies will cause an error and z-stacks are max-intensity projected) and display the [greyscale channels](https://postacquisition.wordpress.com/2015/04/15/see-the-world-in-black-and-white/) with a merged image (keeping original channel LUTs).

![watermark](example.png)

### Updates ###
 - as of commit 19df7b0 the following features have been added:
   - runs on a folder skipping anything with a single channel, movies and anything with a different number of channels to the channel labels given.
   - [optionally] Labels all channels in final montage
   - Files saved out as png files
   
![watermark](example2.png)

### Acknowledgement ###
Written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk).