//@ File (label = "Input directory", style = "directory") input
//@ String (label = "Include suffix") suffix
//@ Boolean (label = "Scale all channels to min/max intensity?") setMinMax
//@ Boolean (label = "Label images?") doLabel
//@ String (label = "Comma separated list of channel names") chanList

//-- SCRIPT TO CREATE AN IMAGE MONTAGE FROM A MULTICHANNEL IMAGE
print("------------------------------------------");
print("Processing folder "+input);
print("------------------------------------------");

//-- preprocess the channel names
chanArr=split(chanList,",");
chanArr=Array.concat(chanArr,"MERGE");
//setBatchMode(true);
processFolder(input,suffix);

print("------------------------------------------");
print("Done");

// ------------------------  FUNCTIONS -----------------------------
function processFolder(input,suffix) {
	list = getFileList(input);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(endsWith(list[i], suffix))
			processFile(input, list[i]);
	}
}

// ------------------------  

function processFile(input, file) {
print("Processing file: " + input + File.separator + file);
//-- Open the file

open(input+File.separator+file);

//-- Get image stats
title=getTitle();
getDimensions(w,h,c,s,f);
bd=bitDepth();

//-- 8 bit files will not retain the original files when processed with some commands so upscale everything
if (bd==8){run("16-bit");}

//-- Catch wrong number of channels if we choose to label
if (doLabel==true){
if (c!=chanArr.length-1){print("     WARNING: Mismatch between number of labels and number of channels");close("*");return;}
}

//-- Catch movies
if (f>1){print("     WARNING: Will not work with movies. ");close("*");return;}

//-- Catch single channel
if (c==1){print("     WARNING: Only one channel found. ");close("*");return;}

//-- If there are more than 1 slices, MAX project
if (s>1){
	print("     More than one slice found. Projecting in 'Z'");
	run("Z Project...", "projection=[Max Intensity]");
	close(title);
	selectWindow("MAX_"+title);
	rename(title);
	}

//-- set min and max intensities
if (setMinMax==true){

	//-- multichannel
	for (chan=0;chan<c;chan++){
	Stack.setChannel(chan+1);
	run("Enhance Contrast", "saturated=0");}
	} else {
	for (chan=0;chan<c-1;chan++){
	Stack.setChannel(chan+1);
	setMinAndMax(0, 30000);	
	}
	//-- Fix the transmitted
	Stack.setChannel(c);
	run("Enhance Contrast", "saturated=0");

} //-- setMinMax Loop

//-- Make the merge
Stack.setDisplayMode("composite");
run("RGB Color");
rename("merge");

//-- Now split the stack
selectWindow(title);
Stack.setDisplayMode("grayscale");
run("RGB Color"); //-- doesn't work on 8-bit images as original is not retained
run("Concatenate...", "  title=Stack image1=["+title+" (RGB)] image2=merge image3=[-- None --]");
close(title);

//-- label slices
if (doLabel==true){
	//-- consider using  getLut(reds, greens, blues) earlier then set foreground colour based on reds[255],greens[255],blues[255]
	setForegroundColor(255, 255, 255);
	setFont("SansSerif", floor(h/10));

	for (chan=0;chan<(c+1);chan++){
		Stack.setSlice(chan+1);
		makeText(chanArr[chan], floor(w*0.05), floor(h*0.85));
		run("Draw", "slice");
	} 
} //-- doLabel?

//-- Make the montage
run("Make Montage...", "scale=0.25 increment=1 border=3 font=12");

//-- save file out
saveAs("PNG",input+File.separator+title+"_montage.png");
close("*");

}