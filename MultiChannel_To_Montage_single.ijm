//@ Boolean (label = "Scale all channels to min/max intensity?") setMinMax
//@ Boolean (label = "Label images?") doLabel
//@ String (label = "Comma separated list of channel names") chanList

//-- SCRIPT TO CREATE AN IMAGE MONTAGE FROM A MULTICHANNEL IMAGE - RUNS ON CURRENTLY SELECTED IMAGE

//-- preprocess the channel names
chanArr=split(chanList,",");
chanArr=Array.concat(chanArr,"MERGE");

//-- Make sure there is an image open
if (nImages()==0){exit("WARNING: No images open!");}

//-- Get image stats
title=getTitle();
getDimensions(w,h,c,s,f);
bd=bitDepth();

//-- 8 bit files will not retain the original files when processed with some commands so upscale everything
if (bd==8){run("16-bit");}

//-- Catch wrong number of channels if we choose to label
if (doLabel==true){
if (c!=chanArr.length-1){exit("WARNING: Mismatch between number of labels and number of channels");}
}

//-- Catch movies
if (f>1){exit("WARNING: Will not work with movies.");}

//-- Catch single channel
if (c==1){exit("WARNING: Only one channel found. ");}

//-- If there are more than 1 slices, MAX project
if (s>1){
	print("More than one slice found. Projecting in 'Z'");
	run("Z Project...", "projection=[Max Intensity]");
	close(title);
	selectWindow("MAX_"+title);
	rename(title);
	}

//-- set min and max intensities
if (setMinMax==true){

	//-- multichannel
	for (chan=0;chan<c;chan++){
	Stack.setChannel(chan+1);
	run("Enhance Contrast", "saturated=0");}
	} else {
	for (chan=0;chan<c-1;chan++){
	Stack.setChannel(chan+1);
	setMinAndMax(0, 30000);	
	}
	//-- Fix the transmitted
	Stack.setChannel(c);
	run("Enhance Contrast", "saturated=0");

} //-- setMinMax Loop

//-- Make the merge
Stack.setDisplayMode("composite");
run("RGB Color");
rename("merge");

//-- Now split the stack
selectWindow(title);
Stack.setDisplayMode("grayscale");
run("RGB Color"); //-- doesn't work on 8-bit images as original is not retained
run("Concatenate...", "  title=Stack image1=["+title+" (RGB)] image2=merge image3=[-- None --]");
close(title);

//-- label slices
if (doLabel==true){
	//-- consider using  getLut(reds, greens, blues) earlier then set foreground colour based on reds[255],greens[255],blues[255]
	setForegroundColor(255, 255, 255);
	setFont("SansSerif", floor(h/10));

	for (chan=0;chan<(c+1);chan++){
		Stack.setSlice(chan+1);
		makeText(chanArr[chan], floor(w*0.05), floor(h*0.85));
		run("Draw", "slice");
	} 
} //-- doLabel?

//-- Make the montage
run("Make Montage...", "scale=1 increment=1 border=3 font=12");
close("Stack");